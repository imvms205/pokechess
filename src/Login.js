import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import FirebaseApp from './FirebaseApp'
const Login = ({ location, history }) => {
  let [username, changeUsername] = useState('')
  let [users, addUsers] = useState([])
  const db = FirebaseApp.firestore()
  useEffect(() => {
    db.collection('Users')
      .get()
      .then(snapshot => {
        addUsers(snapshot.docs.map(doc => doc.data()))
      })
  }, [])
  console.log(history)
  let usersDom = users.map(u => (
    <div>
      <h3>Name: {u.name}</h3>
      <h3>Age: {u.age}</h3>
    </div>
  ))
  return (
    <div id="login_page">
      <h1>Login</h1>
      {usersDom}
      <input value={username} onChange={e => changeUsername(e.target.value)}></input>
      <button
        onClick={() => {
          let uuid = Date.now()
          db.collection('Users')
            .add({
              name: username,
              age: 32,
              id: uuid
            })
            .then(rs => {
              localStorage.setItem('username', username)
              localStorage.setItem('id', uuid)
              location.push('ChessBoard')
            })
        }}
      >
        Add
      </button>
    </div>
  )
}

export default withRouter(Login)
