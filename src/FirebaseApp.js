import firebase from 'firebase'
const config = {
  apiKey: 'AIzaSyAVxcVdipVucHtbu_9xvrY59mMFWYgxupU',
  authDomain: 'pokechess-227f2.firebaseapp.com',
  databaseURL: 'https://pokechess-227f2.firebaseio.com',
  projectId: 'pokechess-227f2',
  storageBucket: 'pokechess-227f2.appspot.com',
  messagingSenderId: '32160852413',
  appId: '1:32160852413:web:120df396e75151ce108ad4'
}
firebase.initializeApp(config)
export default firebase
