import React, { useState, useEffect, useContext } from 'react'
import PokemonContext from './PokemonContext'
import minBy from 'lodash/minBy'
import { cloneNode } from '@babel/types'

const RightBullet = ({ pokemonRef, squarePst, i, name }) => {
  // const [arrPoke, pushArrPoke] = useState([])
  const [bulletPstRight, setBulletPstRight] = useState({ PstX: 0, PstY: 30 })
  let pokemonPst = pokemonRef.current.getBoundingClientRect()
  let pokemonList = useContext(PokemonContext).pokemonList

  let leftPokemonPst = pokemonList
    .filter(pkm => 5 < pkm.position && pkm.position < 24)
    .map(leftpokePst => {
      return leftpokePst.position - 6
    })

  let rotateRightBulletDeg
  let hypotenuseRightBoard = leftPokemonPst.map(leftEle => {
    return {
      x: squarePst[leftEle].positionX - pokemonPst.x,
      y: squarePst[leftEle].positionY - pokemonPst.y,
      z: Math.sqrt(
        Math.pow(squarePst[leftEle].positionX - pokemonPst.x, 2) +
          Math.pow(squarePst[leftEle].positionY - pokemonPst.y, 2)
      )
    }
  })

  let minRightBullet = minBy(hypotenuseRightBoard, function(ele) {
    return ele.z
  })

  let newRightBulletX = minRightBullet.x
  let newRightBulletY = minRightBullet.y
  let newRightBulletZ = minRightBullet.z
  let timeFlyRight
  if (newRightBulletZ <= 200) {
    timeFlyRight = 0.6
  } else if (200 < newRightBulletZ < 300) {
    timeFlyRight = newRightBulletZ / 300
  } else {
    timeFlyRight = newRightBulletZ / 420
  }

  if (newRightBulletY === 0) {
    rotateRightBulletDeg = 180
  } else {
    if (newRightBulletY > 0) {
      rotateRightBulletDeg = 180 - (Math.abs(newRightBulletY) / Math.abs(newRightBulletX)) * 57.29
    } else {
      rotateRightBulletDeg = (Math.abs(newRightBulletY) / Math.abs(newRightBulletZ)) * 57.29 + 180
    }
  }

  useEffect(() => {
    let fire
    if (bulletPstRight.PstX < 0) {
      fire = setTimeout(() => {
        bulletPstRight.PstX = 0
        bulletPstRight.PstY = 30
        setBulletPstRight({ ...bulletPstRight })
      }, `${timeFlyRight * 1000}`)
    } else {
      bulletPstRight.PstX = newRightBulletX + 30
      bulletPstRight.PstY = 30 + newRightBulletY
      setBulletPstRight({ ...bulletPstRight })
    }
    return () => {
      clearTimeout(fire)
    }
  }, [bulletPstRight])

  return (
    <>
      {bulletPstRight.PstX < 0 ? (
        <img
          style={{
            position: 'absolute',
            zIndex: 100,
            maxWidth: '100px',
            maxHeight: '50px',
            left: `${bulletPstRight.PstX}px`,
            top: `${bulletPstRight.PstY}px`,
            transform: `rotate(${rotateRightBulletDeg}deg)`,
            transition: `all ${timeFlyRight}s linear`
          }}
          src={`${process.env.PUBLIC_URL}/images/waterbullet.gif`}
          alt="firebullet"
        />
      ) : (
        <img
          style={{
            position: 'absolute',
            zIndex: 100,
            maxWidth: '100px',
            maxHeight: '50px',
            left: `${bulletPstRight.PstX}px`,
            top: `${bulletPstRight.PstY}px`,
            transform: `rotate(${rotateRightBulletDeg}deg)`
          }}
          src={`${process.env.PUBLIC_URL}/images/waterbullet.gif`}
          alt="firebullet"
        />
      )}
    </>
  )
}

export default RightBullet
