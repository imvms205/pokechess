import React, { useContext } from 'react'
import RoundContext from './RoundContext'
const RoundBar = () => {
  let round = Math.ceil(useContext(RoundContext).round / 2)
  return (
    <div className="roundBar">
      <div id="roundBar">
        <div className="wrapper">
          <div className="round">ROUND</div>
          <div className="number">{round}</div>
        </div>
      </div>
    </div>
  )
}

export default RoundBar
