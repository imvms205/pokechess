import React, { useState, useEffect } from 'react'

const Circle = () => {
  let [time, setTime] = useState(30)
  useEffect(() => {
    let set = setInterval(() => {
      setTime(time => {
        if (time > 1) return time - 1
        return 30
      })
    }, 1000)
    return () => {
      clearInterval(set)
    }
  }, [])
  return (
    <div className="circle">
      <div className={`miniCircle c100 p${Math.ceil((time / 30) * 100)}`}>
        <div className="tinyCircle">{time}</div>
        <div className="slice">
          <div className="bar"></div>
          <div className="fill"></div>
        </div>
      </div>
    </div>
  )
}

export default Circle
