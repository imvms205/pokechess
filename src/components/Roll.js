import React, { useContext } from 'react'
import CardContext from './CardContext'
import BallContext from './BallContext'

const Roll = () => {
  let resetCards = useContext(CardContext).resetCards
  let ball = useContext(BallContext).ball
  let setBall = useContext(BallContext).setBall
  let resetCardss = () => {
    if (ball < 2) return
    resetCards()
    setBall(ball => ball - 2)
  }
  return (
    <div id="roll" onClick={resetCardss}>
      <div>
        <div>Reroll</div>
        <div>
          <div className="numberOfBall">2</div>
          <img
            alt="a"
            className="ballImage"
            src={process.env.PUBLIC_URL + '/images/pokemonBall.png'}
          />
        </div>
      </div>
      <div></div>
    </div>
  )
}
export default Roll
