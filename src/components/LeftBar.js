import React from 'react'
import BuyPokemonBar from './BuyPokemonBar'
import LvBar from './LvBar'
import BallBar from './BallBar'
import '../LeftBar.scss'
import LockBar from './LockBar'
import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

const LeftBar = () => {
  return (
    <DndProvider backend={HTML5Backend}>
      <div id="LeftBar">
        <div id="LeftOfLeftBar">
          <LvBar />
          <BuyPokemonBar />
        </div>
        <div id="RightOfLeftBar">
          <LockBar />
          <BallBar />
        </div>
      </div>
    </DndProvider>
  )
}

export default LeftBar
