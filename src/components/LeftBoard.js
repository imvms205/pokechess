import React from 'react'
import Square from './Square'
import { pokemonExist, findPokemon } from './PokemonFuncs'

function renderSquare(i, zIndex, pokemon) {
  const x = i % 8
  const y = Math.floor(i / 8)
  return pokemon ? (
    <Square key={i} x={x} y={y} i={i} transform="true" zIndex={zIndex} pokemon={pokemon} />
  ) : (
    <Square key={i} x={x} y={y} i={i} transform="true" />
  )
}

const LeftBoard = ({ pokemonList }) => {
  const leftSquares = []
  for (let i = 6; i < 24; i++) {
    if (pokemonExist(pokemonList, i)) {
      let pokemon = findPokemon(pokemonList, i)
      let index
      if (i % 3 === 0) {
        index = i + 2
      } else if (i % 3 === 1) {
        index = i
      } else if (i % 3 === 2) {
        index = i - 2
      }
      leftSquares.push(renderSquare(i, index, pokemon))
    } else {
      leftSquares.push(renderSquare(i))
    }
  }
  return <div id="leftBoard">{leftSquares}</div>
}

export default LeftBoard
