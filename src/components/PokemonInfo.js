export const PokemonInfo = [
  {
    name: 'pichu',
    element: 'thunder',
    price: 1,
    health: 10,
    dame: 1
  },
  {
    name: 'squirtle',
    element: 'water',
    price: 1,
    health: 10,
    dame: 1
  },
  {
    name: 'charmander',
    element: 'fire',
    price: 1,
    health: 10,
    dame: 1
  },
  {
    name: 'bulbasaur',
    element: 'grass',
    price: 1,
    health: 10,
    dame: 1
  },
  {
    name: 'shinx',
    element: 'thunder',
    price: 2,
    health: 15,
    dame: 1
  }
]
export const oneCostPkms = PokemonInfo.filter(pokemon => pokemon.price === 1)
export const twoCostPkms = PokemonInfo.filter(pokemon => pokemon.price === 2)
export const elementOfPokemon = pokemonName => {
  return PokemonInfo.filter(pokemon => {
    return pokemon.name == pokemonName.slice(0, pokemonName.length - 1)
  })[0].element
}

export const arrayWithHealth = pokemonList =>
  pokemonList.map(pokemon => {
    let health = PokemonInfo.filter(
      pkm => pkm.name == pokemon.name.slice(0, pokemon.name.length - 1)
    )[0].health
    pokemon.health = health * pokemon.name.slice(pokemon.name.length - 1, pokemon.name.length)
    return pokemon
  })
export const dame = pokemonName =>
  PokemonInfo.filter(pkm => pkm.name == pokemonName.slice(0, pokemonName.length - 1))[0].dame *
  pokemonName.slice(pokemonName.length - 1, pokemonName.length)
