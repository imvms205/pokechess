export const ItemTypes = {
    'PICHU1': 'pichu1',
    'PICHU2': 'pichu2',
    'PICHU3': 'pichu3',
    'SQUIRTLE1': 'squirtle1',
    'SQUIRTLE2': 'squirtle2',
    'SQUIRTLE3': 'squirtle3',
    'CHARMANDER1': 'charmander1',
    'CHARMANDER2': 'charmander2',
    'CHARMANDER3': 'charmander3',
    'SHINX1': 'shinx1',
    'SHINX2': 'shinx2',
    'SHINX3': 'shinx3',
    'BULBASAUR1': 'bulbasaur1',
    'BULBASAUR2': 'bulbasaur2',
    'BULBASAUR3': 'bulbasaur3',
}
export const arrValue = []
for (let one in ItemTypes) {
    arrValue.push(ItemTypes[one])
}
export const arrKey = []
for (let one in ItemTypes) {
    arrKey.push(one)
}
