import React, { useContext, useRef, useState, useEffect } from 'react'
import { arrValue } from './Constants'
import Pokemon from './Pokemon'
import { useDrop } from 'react-dnd'
import PokemonContext from './PokemonContext'
import SquarePstContext from './SquarePstContext'
import RoundContext from './RoundContext'
import ExpContext from './ExpContext'
import { expAndLv, pokemonExist, canDropToSquare } from './PokemonFuncs'
import LeftBullet from './LeftBullet'
import RightBullet from './RightBullet'

const Square = ({ x, y, i, transform, zIndex, pokemon }) => {
  let pokemonList = useContext(PokemonContext).pokemonList
  let setPokemonList = useContext(PokemonContext).setPokemonList
  let round = useContext(RoundContext).round

  // let arr1 = [8, 10, 13, 15, 16, 18, 21, 23, 24, 26, 29, 31, 32, 34, 37, 39]
  // let arr2 = [40, 42, 45, 47, 48, 50, 53, 55, 56, 58, 61, 63, 64, 66, 69, 71]
  let [, , level] = expAndLv(useContext(ExpContext).exp)
  let numOfCurrentPkm = useContext(PokemonContext).pokemonList.filter(
    pkm => pkm.position >= 6 && pkm.position <= 24
  ).length
  const fill = 'white'
  let havePkmRight = pokemonList.filter(pkm => 23 < pkm.position && pkm.position < 42).length > 0

  let havePkmLeft = pokemonList.filter(pkm => 5 < pkm.position && pkm.position < 24).length > 0

  let squarePst = useContext(SquarePstContext).squarePst
  let pushSquarePst = useContext(SquarePstContext).pushSquarePst
  const coordinateRef = useRef(null)
  useEffect(() => {
    squarePst.push({
      position: i - 6,
      positionX: coordinateRef.current.getBoundingClientRect().x,
      positionY: coordinateRef.current.getBoundingClientRect().y
    })
    pushSquarePst([...squarePst])
    return () => {}
  }, [])

  const [{}, drop] = useDrop({
    accept: arrValue,
    canDrop: () => {
      if (pokemonExist(pokemonList, i)) {
        return canDropToSquare(i, round, numOfCurrentPkm, level, pokemon)
      } else {
        return canDropToSquare(i, round, numOfCurrentPkm, level)
      }
    },
    drop: (item, monitor) => {
      // let dragItem = findPokemon(pokemonList, item.id);
      if (pokemonExist(pokemonList, i)) {
        // let dropItem = findPokemon(pokemonList, i);
        // let dragItem = item;
        let newPokemonList = pokemonList.map(ele => {
          if (ele.position === i) {
            ele.position = item.id
          } else if (ele.position === item.id) {
            ele.position = i
          }
          return ele
        })
        setPokemonList(newPokemonList)
      } else {
        let newPokemonList = pokemonList.map((ele, index) => {
          if (ele.position === item.id) {
            ele.position = i
          }
          return ele
        })
        setPokemonList(newPokemonList)
      }
    },
    collect: monitor => ({
      canDrop: !!monitor.canDrop()
    })
  })

  return (
    <div
      ref={drop}
      className="square"
      idd={zIndex}
      style={
        !zIndex
          ? {
              backgroundColor: fill,
              justifyContent: 'flex-end'
            }
          : {
              backgroundColor: fill,
              justifyContent: 'center',
              zIndex: zIndex
            }
      }
    >
      <div ref={coordinateRef} className="miniSquare">
        {pokemon && <Pokemon i={i} transform={transform} pokemon={pokemon} />}
        {i < 24 && pokemon && squarePst.length && round % 2 === 0 && havePkmRight && (
          <LeftBullet
            squarePst={squarePst}
            name={pokemon.name}
            i={i - 6}
            pokemonRef={coordinateRef}
          />
        )}
        {i >= 24 && pokemon && squarePst.length && round % 2 === 0 && havePkmLeft && (
          <RightBullet
            squarePst={squarePst}
            name={pokemon.name}
            i={i - 6}
            pokemonRef={coordinateRef}
          />
        )}
        {/* {pokemon && squarePst.length && round % 2 === 0 && havePkmLeft && (
          
        )} */}
      </div>
    </div>
  )
}
export default Square
