export const pokemonExist = (pokemonList, index) =>
  pokemonList.filter(item => item.position === index).length > 0
export const findPokemon = (pokemonList, index) =>
  pokemonList.filter(item => item.position === index)[0]
export const deletePokemon = (pokemonList, index) =>
  pokemonList.filter(item => item.position !== index)
export const canDropToSquare = (i, round, current, max, pokemon) => {
  if (i < 24) {
    if (round % 2 === 1) {
      if (pokemon) {
        return true
      } else {
        if (current + 1 <= max) {
          return true
        } else {
          return false
        }
      }
    } else {
      return false
    }
  } else {
    return false
  }
}
export const canDropToHolder = (drop, drag, round) => {
  if (drop < 24) {
    if (round % 2 === 0) {
      if (drag.id < 6) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
  } else {
    return false
  }
}
export const expAndLv = totalExp => {
  let currentExp
  let maxExp
  let level
  if (totalExp < 2) {
    maxExp = 2
    currentExp = totalExp
    level = 1
  } else if (totalExp >= 2 && totalExp < 6) {
    maxExp = 4
    currentExp = totalExp - 2
    level = 2
  } else if (totalExp >= 6 && totalExp < 12) {
    maxExp = 6
    currentExp = totalExp - 6
    level = 3
  } else if (totalExp >= 12 && totalExp < 24) {
    maxExp = 12
    currentExp = totalExp - 12
    level = 4
  } else if (totalExp >= 24 && totalExp < 44) {
    maxExp = 20
    currentExp = totalExp - 24
    level = 5
  } else if (totalExp >= 44 && totalExp < 76) {
    maxExp = 32
    currentExp = totalExp - 44
    level = 6
  } else if (totalExp >= 76 && totalExp < 126) {
    maxExp = 50
    currentExp = totalExp - 76
    level = 7
  } else if (totalExp >= 126 && totalExp <= 196) {
    maxExp = 70
    currentExp = totalExp - 126
    level = 8
  } else {
    maxExp = 70
    currentExp = 70
    level = 9
  }
  return [maxExp, currentExp, level]
}
