import React, { useContext } from 'react'
import BallContext from './BallContext'

const BallBar = () => {
  return (
    <div id="BallBar">
      <div className="wrapper">
        <div className="number">{useContext(BallContext).ball}</div>
        <div className="ball">
          <img
            className="ballImage"
            alt="a"
            src={process.env.PUBLIC_URL + '/images/pokemonBall.png'}
          />
        </div>
      </div>
    </div>
  )
}

export default BallBar
