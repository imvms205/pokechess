import React from 'react'
import RollAndBuy from './RollAndBuy'
import PokemonCardWrapper from './PokemonCardWrapper'

const BuyPokemonBar = () => {
  return (
    <div id="BuyPokemonBar">
      <RollAndBuy />
      <PokemonCardWrapper />
    </div>
  )
}

export default BuyPokemonBar
