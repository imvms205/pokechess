import React, { useContext } from 'react'
import ExpContext from './ExpContext'
import BallContext from './BallContext'
import { expAndLv } from './PokemonFuncs'

const Buy = () => {
  let [maxExp, currentExp] = expAndLv(useContext(ExpContext).exp)
  let setExp = useContext(ExpContext).setExp
  let ball = useContext(BallContext).ball
  let setBall = useContext(BallContext).setBall
  let handleBuy = () => {
    if (ball < 4) return
    setExp(exp => exp + 4)
    setBall(ball => ball - 4)
  }
  return (
    <div id="buy" onClick={handleBuy}>
      <div>
        <div>Buy XP</div>
        <div>
          <div className="numberOfBall">4</div>
          <img
            alt="a"
            className="ballImage"
            src={process.env.PUBLIC_URL + '/images/pokemonBall.png'}
          />
        </div>
      </div>
      <div className="showXP">
        <div className="showNumber">
          {currentExp}/{maxExp}
        </div>
        <div className="showProgress">
          <div
            className="percent"
            style={{
              width: `${(currentExp / maxExp) * 60}px`
            }}
          ></div>
        </div>
      </div>
    </div>
  )
}
export default Buy
