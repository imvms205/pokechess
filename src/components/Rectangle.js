import React, { useContext } from 'react'
import PokemonContext from './PokemonContext'
import RoundContext from './RoundContext'

import ExpContext from './ExpContext'
import { expAndLv } from './PokemonFuncs'

const Rectangle = () => {
  let round = useContext(RoundContext).round
  let [, , level] = expAndLv(useContext(ExpContext).exp)
  let numOfCurrentPkm = useContext(PokemonContext).pokemonList.filter(
    pkm => pkm.position >= 6 && pkm.position <= 24
  ).length
  return (
    <div className="rectangle">
      <div className="currentStatus">{round % 2 === 1 ? 'Planning' : 'Fighting'}</div>
      <div className="currentPkms">
        <img
          alt="a"
          className="pokemonIcon"
          src={`${process.env.PUBLIC_URL}/images/pokemonIcon.png`}
        />
        <div className="numberOfPkms">
          {numOfCurrentPkm} / {level}
        </div>
      </div>
    </div>
  )
}

export default Rectangle
