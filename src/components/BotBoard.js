import React from 'react'
import HolderOfMe from './HolderOfMe'
import Board from './Board'
import HolderOfOpponent from './HolderOfOpponent'

const BotBoard = ({ pokemonList }) => {
    return (
        <div id='botBoard'>
            <HolderOfMe pokemonList={pokemonList} />
            <Board pokemonList={pokemonList} />
            <HolderOfOpponent pokemonList={pokemonList} />
        </div>
    )
}

export default BotBoard