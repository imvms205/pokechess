import React from 'react'
import Board from './Board'
import { DndProvider } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'
import '../OuterBoard.scss'
import HolderOfOpponent from './HolderOfOpponent'
import HolderOfMe from './HolderOfMe'

const OuterBoard = ({ pokemonList }) => {
  return (
    <DndProvider backend={HTML5Backend}>
      <div id="outerBoard">
        <HolderOfMe pokemonList={pokemonList} />
        <Board pokemonList={pokemonList} />
        <HolderOfOpponent pokemonList={pokemonList} />
      </div>
    </DndProvider>
  )
}
export default OuterBoard
