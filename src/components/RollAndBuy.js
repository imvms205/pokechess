import React from 'react'
import Roll from './Roll'
import Buy from './Buy'
const RollAndBuy = () => {
  return (
    <div id="rollAndBuy">
      <Roll />
      <Buy />
    </div>
  )
}

export default RollAndBuy
