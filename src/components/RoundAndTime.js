import React from 'react'
import '../RoundAndTime.scss'
import '../circle.scss'
import RoundBar from './RoundBar'
import Rectangle from './Rectangle'
import Circle from './Circle'

const RoundAndTime = () => {
  return (
    <div id="roundAndTime">
      <RoundBar />
      <div className="timeBar">
        <Rectangle />
        <Circle />
      </div>
    </div>
  )
}

export default RoundAndTime
