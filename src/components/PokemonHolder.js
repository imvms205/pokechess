import React, { useContext } from 'react'
import Pokemon from './Pokemon'
import { useDrop } from 'react-dnd'
import { arrValue } from './Constants'
import PokemonContext from './PokemonContext'
import RoundContext from './RoundContext'
import { pokemonExist, canDropToHolder } from './PokemonFuncs'

const PokemonHolder = ({ i, transform, pokemon }) => {
  let round = useContext(RoundContext).round
  let pokemonList = useContext(PokemonContext).pokemonList
  let setPokemonList = useContext(PokemonContext).setPokemonList
  const [, drop] = useDrop({
    accept: arrValue,
    canDrop: item => canDropToHolder(i, item, round),
    drop: (item, monitor) => {
      if (pokemonExist(pokemonList, i)) {
        let newPokemonList = pokemonList.map(ele => {
          if (ele.position === i) {
            ele.position = item.id
          } else if (ele.position === item.id) {
            ele.position = i
          }
          return ele
        })
        setPokemonList(newPokemonList)
      } else {
        let newPokemonList = pokemonList.map((ele, index) => {
          if (ele.position === item.id) {
            ele.position = i
          }
          return ele
        })
        setPokemonList(newPokemonList)
      }
    },
    collect: monitor => ({
      canDrop: !!monitor.canDrop()
    })
  })

  return (
    <div className="holderSquare" ref={drop}>
      <img
        className="frame"
        draggable="false"
        alt="bla"
        id={i}
        src={process.env.PUBLIC_URL + '/images/pkm_holder.png'}
      />
      {pokemon && <Pokemon i={i} transform={transform} pokemon={pokemon} />}
    </div>
  )
}
export default PokemonHolder
