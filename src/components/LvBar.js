import React, { useContext } from 'react'
import ExpContext from './ExpContext'
import { expAndLv } from './PokemonFuncs'

const LvBar = () => {
  let [, , level] = expAndLv(useContext(ExpContext).exp)
  return (
    <div id="LvBar">
      <div className="wrapper">
        <div className="lv">LV</div>
        <div className="number">{level}</div>
      </div>
    </div>
  )
}

export default LvBar
