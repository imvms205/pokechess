import React from 'react'
import '../PlayersBar.scss'
import PlayerInfo from './PlayerInfo'

const renderPlayerInfo = i => {
  return <PlayerInfo key={i} />
}

const PlayersBar = () => {
  let infos = []
  for (let i = 0; i < 4; i++) {
    infos.push(renderPlayerInfo(i))
  }
  return (
    <div id="PlayersBar">
      <div id="PlayersBarWrapper">{infos}</div>
    </div>
  )
}

export default PlayersBar
