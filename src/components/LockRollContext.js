import { createContext } from 'react'

const LockRollContext = createContext()

export default LockRollContext
