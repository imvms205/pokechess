import React, { useState, useEffect, useContext } from 'react'
import PokemonContext from './PokemonContext'
import minBy from 'lodash/minBy'
import { elementOfPokemon, dame } from './PokemonInfo'

const LeftBullet = ({ pokemonRef, squarePst, i, name }) => {
  // const [arrPoke, pushArrPoke] = useState([])
  const [bulletPstLeft, setBulletPstLeft] = useState({ PstX: 30, PstY: 30 })
  let pokemonPst = pokemonRef.current.getBoundingClientRect()
  let pokemonList = useContext(PokemonContext).pokemonList
  let setPokemonList = useContext(PokemonContext).setPokemonList

  let rightPokemonPst = pokemonList
    .filter(pkm => 23 < pkm.position && pkm.position < 42)
    .map(rightpokePst => {
      return rightpokePst.position - 6
    })

  let newLeftBulletX, newLeftBulletY, rotateLeftBulletDeg, newLeftBulletZ
  let hypotenuseLeftBoard = rightPokemonPst.map(rightEle => {
    return {
      position: rightEle,
      x: squarePst[rightEle].positionX - pokemonPst.x,
      y: squarePst[rightEle].positionY - pokemonPst.y,
      z: Math.sqrt(
        Math.pow(squarePst[rightEle].positionX - pokemonPst.x, 2) +
          Math.pow(squarePst[rightEle].positionY - pokemonPst.y, 2)
      )
    }
  })

  let minLeftBullet = minBy(hypotenuseLeftBoard, function(ele) {
    return ele.z
  })

  newLeftBulletX = minLeftBullet.x
  newLeftBulletY = minLeftBullet.y
  newLeftBulletZ = minLeftBullet.z
  let timeFlyLeft
  if (newLeftBulletZ <= 200) {
    timeFlyLeft = 0.6
  } else if (200 < newLeftBulletZ < 300) {
    timeFlyLeft = newLeftBulletZ / 300
  } else {
    timeFlyLeft = newLeftBulletZ / 420
  }

  if (newLeftBulletY > 0) {
    rotateLeftBulletDeg = (newLeftBulletY / newLeftBulletZ) * 57.29578
  } else {
    if (newLeftBulletY === 0) {
      rotateLeftBulletDeg = 0
    } else {
      rotateLeftBulletDeg = (newLeftBulletY / newLeftBulletX) * 57.29578
    }
  }

  let opponentPosition = minLeftBullet.position + 6
  let timeToReceiveDame = timeFlyLeft
  let dameWillReceive = dame(name)
  let needToSetState = false
  let newPokemonList = pokemonList.map(pkm => {
    if (pkm.position == opponentPosition) {
      if (pkm.timeToReceiveDame == null) {
        needToSetState = true
      } else {
      }
      pkm.timeToReceiveDame = timeToReceiveDame * 1000
      pkm.dameWillReceive = dameWillReceive
    }
    return pkm
  })
  if (needToSetState) {
    setPokemonList(newPokemonList)
    console.log('set')
  }
  useEffect(() => {
    let fire
    if (bulletPstLeft.PstX > 30) {
      fire = setTimeout(() => {
        bulletPstLeft.PstX = 30
        bulletPstLeft.PstY = 30
        setBulletPstLeft({ ...bulletPstLeft })
      }, `${timeFlyLeft * 1000}`)
    } else {
      bulletPstLeft.PstX = newLeftBulletX
      bulletPstLeft.PstY = 30 + newLeftBulletY
      setBulletPstLeft({ ...bulletPstLeft })
    }
    return () => {
      clearTimeout(fire)
    }
  }, [bulletPstLeft])
  let element = elementOfPokemon(name)
  return (
    <>
      {bulletPstLeft.PstX > 30 ? (
        <img
          style={{
            position: 'absolute',
            zIndex: 100,
            maxWidth: '100px',
            maxHeight: '50px',
            left: `${bulletPstLeft.PstX}px`,
            top: `${bulletPstLeft.PstY}px`,
            transform: `rotate(${rotateLeftBulletDeg}deg)`,
            transition: `all ${timeFlyLeft}s linear`
          }}
          src={`${process.env.PUBLIC_URL}/images/${element}bullet.gif`}
          alt="firebullet"
        />
      ) : (
        <img
          style={{
            position: 'absolute',
            zIndex: 100,
            maxWidth: '100px',
            maxHeight: '50px',
            left: `${bulletPstLeft.PstX}px`,
            top: `${bulletPstLeft.PstY}px`,
            transform: `rotate(${rotateLeftBulletDeg}deg)`
          }}
          src={`${process.env.PUBLIC_URL}/images/${element}bullet.gif`}
          alt="firebullet"
        />
      )}
    </>
  )
}

export default LeftBullet
