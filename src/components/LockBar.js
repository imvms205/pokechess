import React, { useContext } from 'react'
import LockRollContext from './LockRollContext'

const LockBar = () => {
  let lockRoll = useContext(LockRollContext).lockRoll
  let setLockRoll = useContext(LockRollContext).setLockRoll
  const toggleLock = e => {
    setLockRoll(!lockRoll)
    if (e.target.classList.contains('locked')) {
      if (e.target.classList.contains('ballImage')) {
        e.target.src = process.env.PUBLIC_URL + '/images/unlock.png'
      } else {
        e.target.querySelector('.ballImage').src = process.env.PUBLIC_URL + '/images/unlock.png'
      }
    } else {
      if (e.target.classList.contains('ballImage')) {
        e.target.src = process.env.PUBLIC_URL + '/images/lock.png'
      } else {
        e.target.querySelector('.ballImage').src = process.env.PUBLIC_URL + '/images/lock.png'
      }
    }
    e.target.classList.toggle('locked')
  }
  return (
    <div id="LockBar" onClick={toggleLock}>
      <div className="wrapper">
        <div className="ball">
          <img alt="a" className=" ballImage" src={process.env.PUBLIC_URL + '/images/unlock.png'} />
        </div>
      </div>
    </div>
  )
}

export default LockBar
