import React, { useContext } from 'react'
import PokemonContext from './PokemonContext'
import BallContext from './BallContext'
import CardContext from './CardContext'
import RoundContext from './RoundContext'
import { arrayWithHealth } from './PokemonInfo'
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1)
}

const PokemonCard = ({ pokemon, ii }) => {
  let pokemonList = useContext(PokemonContext).pokemonList
  let setPokemonList = useContext(PokemonContext).setPokemonList
  let ball = useContext(BallContext).ball
  let setBall = useContext(BallContext).setBall
  let cards = useContext(CardContext).cards
  let setCards = useContext(CardContext).setCards
  let round = useContext(RoundContext).round
  let pokemon_name = pokemon.name
  let pokemon_price = pokemon.price

  const handleClick = () => {
    if (ball < pokemon.price) return
    let pokemonsInHolder = pokemonList.filter(pokemon => pokemon.position < 6)
    let slotLeft = 6 - pokemonsInHolder.length
    if (slotLeft > 0) {
      for (let i = 0; i < 6; i++) {
        let putable =
          pokemonsInHolder.filter(pokemon => pokemon.position === i).length > 0 ? false : true
        if (round % 2 === 0) {
          putable = false
        }
        if (putable === true) {
          let newCards = cards.filter((card, index) => index !== ii)
          setCards(newCards)
          setBall(ball => ball - pokemon.price)

          let name = pokemon_name + '1'
          let position
          let newPokemon
          let leftPokemonList = pokemonList.filter(pkm => pkm.position < 24)
          let rightPokemonList = pokemonList.filter(pkm => pkm.position >= 24)
          let needToEvolveTo2 = leftPokemonList.filter(pkm => pkm.name === name).length === 2
          let needToEvolveTo3 =
            leftPokemonList.filter(pkm => pkm.name === pokemon_name + '2').length === 2
          let newPokemonList

          // console.log(name)
          // console.log(leftPokemonList)
          // console.log(leftPokemonList.filter(pkm => pkm.name===name))

          if (needToEvolveTo2 && round % 2 === 1) {
            if (needToEvolveTo3) {
              // console.log('a')
              let name1 = pokemon_name + '1'
              let name2 = pokemon_name + '2'
              let maxPosition = Math.max(
                ...leftPokemonList
                  .filter(pkm => pkm.name === name1 || pkm.name === name2)
                  .map(pkm => pkm.position)
              )

              let pkmWillEvovle = leftPokemonList.filter(pkm => pkm.position === maxPosition)[0]

              name = pokemon_name + 3
              position = pkmWillEvovle.position

              newPokemon = {
                name: name,
                position: position
              }
              leftPokemonList = leftPokemonList
                .filter(pkm => pkm.name !== name1)
                .filter(pkm => pkm.name !== name2)
              newPokemonList = [...leftPokemonList, ...rightPokemonList, newPokemon]
            } else {
              let maxPosition = Math.max(
                ...leftPokemonList.filter(pkm => pkm.name === name).map(pkm => pkm.position)
              )
              let pkmWillEvovle = leftPokemonList.filter(pkm => pkm.position === maxPosition)[0]

              name = pokemon_name + 2
              position = pkmWillEvovle.position

              newPokemon = {
                name: name,
                position: position
              }
              leftPokemonList = leftPokemonList.filter(pkm => pkm.name !== pokemon_name + '1')
              newPokemonList = [...leftPokemonList, ...rightPokemonList, newPokemon]
            }
          } else {
            name = pokemon_name + '1'
            position = i

            newPokemon = {
              name: name,
              position: position
            }

            newPokemonList = [...pokemonList, newPokemon]
          }
          // console.log(newPokemonList)
          setPokemonList(arrayWithHealth(newPokemonList))

          break
        }
      }
    } else {
      alert('full slot')
    }
  }

  return (
    <div className="pokemonCard" onClick={handleClick}>
      <div className="pokemonCardWrapper">
        <div className="pokemonImage">
          <img alt="a" src={`${process.env.PUBLIC_URL}/images/background/${pokemon_name}1Bg.png`} />
        </div>
        <div className="pokemonInfo">
          <div className="pokemonName">{capitalizeFirstLetter(pokemon_name)}</div>
          <div className="pokemonPrice">
            <div className="numberOfBall">{pokemon_price}</div>
            <img
              alt="a"
              className="ballImage"
              src={process.env.PUBLIC_URL + '/images/pokemonBall.png'}
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default PokemonCard
