import React, { useContext } from 'react'
import { ItemTypes } from './Constants'
import { useDrag } from 'react-dnd'
import RoundContext from './RoundContext'
import PokemonContext from './PokemonContext'
import { PokemonInfo } from './PokemonInfo'

const Pokemon = ({ i, transform, pokemon }) => {
  let pokemonName = pokemon.name
  let currentHealth = pokemon.health

  let maxHealth =
    PokemonInfo.filter(pkm => pkm.name == pokemonName.slice(0, pokemonName.length - 1))[0].health *
    pokemonName.slice(pokemonName.length - 1, pokemonName.length)
  let round = useContext(RoundContext).round
  let pokemonList = useContext(PokemonContext).pokemonList
  // let setPokemonList = useContext(PokemonContext).setPokemonList
  // let timeToReceiveDame = pokemon.timeToReceiveDame
  let dameWillReceive = pokemon.dameWillReceive
  // console.log('dameWillReceive : ', dameWillReceive)
  if (dameWillReceive != null) {
    let pokemonListAfterShoot
    pokemonListAfterShoot = pokemonList.map(pkm => {
      if (pkm.position == i) {
        pkm.health -= dameWillReceive
        pkm.dameWillReceive = null
        // console.log('pkm : ', pkm)
      }
      return pkm
    })
    // setPokemonList(pokemonListAfterShoot)
  }
  // console.log('pokemonListAfterShoot : ', pokemonListAfterShoot)
  let havePkmRight = pokemonList.filter(pkm => 23 < pkm.position && pkm.position < 42).length > 0
  let havePkmLeft = pokemonList.filter(pkm => 5 < pkm.position && pkm.position < 24).length > 0
  const [{ isDragging, canDrag }, drag] = useDrag({
    item: {
      type: ItemTypes[pokemonName.toUpperCase()],
      id: i,
      price: parseInt(pokemonName.slice(pokemonName.length - 1, pokemonName.length))
    },
    canDrag: () => {
      // let roundd = useContext(RoundContext).round
      if (i < 24) {
        // if (i < 6) {
        return true
        // } else {
        //   if (round % 2 == 1) {
        //     return true
        //   } else {
        //     return false
        //   }
        // }
      } else {
        return false
      }
    },
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
      canDrag: !!monitor.canDrag()
    })
  })
  let status
  if (i > 5) {
    if (havePkmRight && havePkmLeft) {
      if (round % 2 == 1) {
        status = 'idle'
      } else {
        status = 'attack'
      }
    } else {
      status = 'idle'
    }
  } else {
    status = 'idle'
  }

  return (
    <>
      <div className="healthBar">
        <div className="innerHealthBar">
          <div
            className="healthPart"
            style={{
              width: (currentHealth / maxHealth) * 100 + '%'
            }}
          ></div>
        </div>
      </div>
      {canDrag ? (
        <img
          alt="bla"
          className="pkm"
          src={`${process.env.PUBLIC_URL}/images/${status}/${pokemon.name}.gif`}
          ref={drag}
          style={
            transform
              ? {
                  opacity: isDragging ? 0 : 1,
                  cursor: 'move'
                }
              : {
                  opacity: isDragging ? 0 : 1,
                  cursor: 'move',
                  transform: 'scaleX(-1)'
                }
          }
        />
      ) : (
        <img
          className="pkm"
          draggable="false"
          alt="bla"
          notdrag="yeah"
          src={`${process.env.PUBLIC_URL}/images/${status}/${pokemon.name}.gif`}
          style={
            transform
              ? {
                  opacity: isDragging ? 1 : 1
                }
              : {
                  transform: 'scaleX(-1)'
                }
          }
        />
      )}
    </>
  )
}

export default Pokemon
