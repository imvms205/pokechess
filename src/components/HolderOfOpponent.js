import React from 'react'
import PokemonHolder from './PokemonHolder'
import { pokemonExist, findPokemon } from './PokemonFuncs'

const HolderOfOpponent = ({ pokemonList }) => {
  const holderOfOpponent = []
  for (let i = 42; i < 48; i++) {
    if (pokemonExist(pokemonList, i)) {
      let pokemon = findPokemon(pokemonList, i)
      holderOfOpponent.push(<PokemonHolder key={i} i={i} pokemon={pokemon} />)
    } else {
      holderOfOpponent.push(<PokemonHolder key={i} i={i} />)
    }
  }
  return <div className="holderOfOpponent">{holderOfOpponent}</div>
}

export default HolderOfOpponent
