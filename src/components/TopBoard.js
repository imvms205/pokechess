import React from 'react'

const TopBoard = () => {
    return (
        <div id='topBoard'>
            <div id='roundBar'>
                <div className='wrapper'>
                    <div className='round'>
                        ROUND
                    </div>
                    <div className='number'>
                        5
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TopBoard