import React, { useState } from 'react'

import LeftBoard from './LeftBoard'
import RightBoard from './RightBoard'
import SquarePstContext from './SquarePstContext'

const Board = ({ pokemonList }) => {
  let [squarePst, pushSquarePst] = useState([])
  let squarePstArr = {
    squarePst: squarePst,
    pushSquarePst: pushSquarePst
  }
  return (
    <SquarePstContext.Provider value={squarePstArr}>
      <div id="board">
        <LeftBoard pokemonList={pokemonList} />
        <RightBoard pokemonList={pokemonList} />
      </div>
    </SquarePstContext.Provider>
  )
}
export default Board
