import React, { useContext } from 'react'
import PokemonCard from './PokemonCard'
import CardContext from './CardContext'
import RoundContext from './RoundContext'
import BallContext from './BallContext'
import PokemonContext from './PokemonContext'
import { useDrop } from 'react-dnd'
import { arrValue } from './Constants'

const renderCard = (i, cards, setCards) => {
  return <PokemonCard ii={i} pokemon={cards[i]} key={i} />
}

const PokemonCardWrapper = () => {
  let cardArray = []
  let cards = useContext(CardContext).cards
  let setCards = useContext(CardContext).setCards
  let ball = useContext(BallContext).ball
  let setBall = useContext(BallContext).setBall
  let round = useContext(RoundContext).round
  let pokemonList = useContext(PokemonContext).pokemonList
  let setPokemonList = useContext(PokemonContext).setPokemonList
  for (let i = 0; i < cards.length; i++) {
    cardArray.push(renderCard(i, cards, setCards))
  }

  const [{}, dropp] = useDrop({
    accept: arrValue,
    canDrop: item => {
      if (item.id < 6) {
        return true
      } else {
        if (round % 2 == 1) {
          return true
        } else {
          return false
        }
      }
    },
    drop: (item, monitor) => {
      let newPokemonList = pokemonList.filter(ele => ele.position != item.id)
      setPokemonList(newPokemonList)
      setBall(ball => ball + item.price)
    },
    collect: monitor => ({
      canDrop: !!monitor.canDrop()
    })
  })

  return (
    <div ref={dropp}>
      {round % 2 === 0 && <div className="overLay"></div>}
      <div id="pokemonCardWrapper">{cardArray}</div>
    </div>
  )
}

export default PokemonCardWrapper
