import React from 'react'
import Square from './Square'
import { pokemonExist, findPokemon } from './PokemonFuncs'
function renderSquare(i, pokemon) {
  const x = i % 8
  const y = Math.floor(i / 8)
  return pokemon ? (
    <Square key={i} x={x} y={y} i={i} pokemon={pokemon} />
  ) : (
    <Square key={i} x={x} y={y} i={i} />
  )
}

const RightBoard = ({ pokemonList }) => {
  const rightSquares = []
  for (let i = 24; i < 42; i++) {
    if (pokemonExist(pokemonList, i)) {
      let pokemon = findPokemon(pokemonList, i)
      rightSquares.push(renderSquare(i, pokemon))
    } else {
      rightSquares.push(renderSquare(i))
    }
  }
  return <div id="rightBoard">{rightSquares}</div>
}

export default RightBoard
