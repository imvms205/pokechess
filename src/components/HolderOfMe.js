import React from 'react'
import PokemonHolder from './PokemonHolder'
import { pokemonExist, findPokemon } from './PokemonFuncs'

const HolderOfMe = ({ pokemonList }) => {
  const holderOfMe = []
  for (let i = 0; i < 6; i++) {
    if (pokemonExist(pokemonList, i)) {
      let pokemon = findPokemon(pokemonList, i)
      holderOfMe.push(<PokemonHolder key={i} i={i} transform="true" pokemon={pokemon} />)
    } else {
      holderOfMe.push(<PokemonHolder key={i} i={i} transform="true" />)
    }
  }
  return <div className="holderOfMe">{holderOfMe}</div>
}

export default HolderOfMe
