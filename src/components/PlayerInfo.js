import React from 'react'

const PlayerInfo = () => {
  return (
    <div className="playerInfo">
      <div className="playerInfoWrapper">
        <div className="left">
          <div className="leftWrapper">
            <div className="name">Shi</div>
            <div className="healthBar">
              <div className="hp">HP</div>
              <div className="hpBar">
                <div className="percentHpBar"></div>
              </div>
            </div>
            <div className="healthNum">112/ 112</div>
          </div>
        </div>
        <div className="right">
          <img className="avatarPlayer" src={`${process.env.PUBLIC_URL}/images/avatarPlayer.png`} />
          <div className="lv">
            <div className="lvWrapper">
              <div className="lvWrapperWrapper">Lv.30</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PlayerInfo
