import React, { useState, useEffect } from 'react'
import './App.css'
import OuterBoard from './components/OuterBoard'
import PlayersBar from './components/PlayersBar'
import LeftBar from './components/LeftBar'
import PokemonContext from './components/PokemonContext'
import RoundContext from './components/RoundContext'
import ExpContext from './components/ExpContext'
import BallContext from './components/BallContext'
import CardContext from './components/CardContext'
import MaxPkmContext from './components/MaxPkmContext'
import LockRollContext from './components/LockRollContext'
import RoundAndTime from './components/RoundAndTime'
import { oneCostPkms, twoCostPkms, arrayWithHealth } from './components/PokemonInfo'
import { expAndLv } from './components/PokemonFuncs'

const generateCards = level => {
  let randomCardPkms = []
  let random
  let randomOne = () => Math.floor(Math.random() * oneCostPkms.length)
  let randomTwo = () => Math.floor(Math.random() * twoCostPkms.length)
  for (let i = 0; i < 5; i++) {
    random = Math.random()
    if (level === 0) {
      if (random < 1) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 1) {
      if (random < 0.9) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 2) {
      if (random < 0.8) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 3) {
      if (random < 0.7) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 4) {
      if (random < 0.6) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 5) {
      if (random < 0.5) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 6) {
      if (random < 0.4) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 7) {
      if (random < 0.3) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else if (level === 8) {
      if (random < 0.2) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    } else {
      if (random < 0.2) {
        randomCardPkms.push(oneCostPkms[randomOne()])
      } else {
        randomCardPkms.push(twoCostPkms[randomTwo()])
      }
    }
  }
  return randomCardPkms
}
function App() {
  let initialPokemonList = [
    {
      name: 'charmander1',
      position: 6,
      timeToReceiveDame: null,
      dameWillReceive: null
    },
    {
      name: 'charmander3',
      position: 9,
      timeToReceiveDame: null,
      dameWillReceive: null
    },
    {
      name: 'charmander1',
      position: 12,
      timeToReceiveDame: null,
      dameWillReceive: null
    },
    {
      name: 'squirtle3',
      position: 15,
      timeToReceiveDame: null,
      dameWillReceive: null
    },
    {
      name: 'charmander1',
      position: 32,
      timeToReceiveDame: null,
      dameWillReceive: null
    }
  ]
  initialPokemonList = arrayWithHealth(initialPokemonList)
  initialPokemonList[0].health = 5
  initialPokemonList[1].health = 8
  let [pokemonList, setPokemonList] = useState(initialPokemonList)
  let [round, setRound] = useState(1)
  let [exp, setExp] = useState(30)
  let [ball, setBall] = useState(100)
  let [maxPkm, setMaxPkm] = useState(0)
  let [, , level] = expAndLv(exp)
  let [cards, setCards] = useState(generateCards(level))
  let [lockRoll, setLockRoll] = useState(false)
  // console.log('pokemonList : ', pokemonList)
  const resetCards = () => {
    setCards(generateCards(level))
  }

  useEffect(() => {
    let interval = setInterval(() => {
      let roundState
      let lockRollState

      setRound(round => {
        roundState = round + 1
        return round + 1
      })
      if (roundState % 2 === 1) {
        if (exp < 70) {
          setExp(exp => {
            return exp + 2
          })
        }
        setLockRoll(lockRoll => {
          lockRollState = lockRoll
          return lockRoll
        })
        if (lockRollState === false) {
          resetCards()
        }
        setPokemonList(pokemonList => {
          return pokemonList
        })
      }
    }, 5000)
    return () => {
      clearInterval(interval)
    }
  }, [])

  return (
    <PokemonContext.Provider value={{ pokemonList: pokemonList, setPokemonList: setPokemonList }}>
      <RoundContext.Provider value={{ round: round, setRound: setRound }}>
        <ExpContext.Provider value={{ exp: exp, setExp: setExp }}>
          <BallContext.Provider value={{ ball: ball, setBall: setBall }}>
            <CardContext.Provider
              value={{ cards: cards, setCards: setCards, resetCards: resetCards }}
            >
              <MaxPkmContext.Provider value={{ maxPkm: maxPkm, setMaxPkm: setMaxPkm }}>
                <LockRollContext.Provider value={{ lockRoll: lockRoll, setLockRoll: setLockRoll }}>
                  <RoundAndTime />
                  <LeftBar />
                  <OuterBoard pokemonList={pokemonList} />
                  <PlayersBar />
                </LockRollContext.Provider>
              </MaxPkmContext.Provider>
            </CardContext.Provider>
          </BallContext.Provider>
        </ExpContext.Provider>
      </RoundContext.Provider>
    </PokemonContext.Provider>
  )
}

export default App
