import React from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import ChessBoard from './ChessBoard'
import Login from './Login'

function AppRouter() {
  return (
    <Router>
      <>
        <Route path="/" exact component={Login} />
        <Route path="/ChessBoard/" exact component={ChessBoard} />
      </>
    </Router>
  )
}

export default AppRouter
